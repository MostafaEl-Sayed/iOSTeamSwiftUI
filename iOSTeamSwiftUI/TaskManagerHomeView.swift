//
//  TaskManagerHomeView.swift
//  iOSTeamSwiftUI
//
//  Created by Mostafa El_sayed on 01/07/2024.
//

import SwiftUI

struct Task: Identifiable {
    let id = UUID()
    let name: String
    let dueDate: String
}


struct TaskManagerHomeView:  View {
    let tasks = [
        Task(name: "Buy vegetables from the marketBuy", dueDate: "Tomorrow"),
        Task(name: "Finish university project", dueDate: "Next week"),
        Task(name: "Call Ahmed", dueDate: "Today"),
        Task(name: "Take the kids to the park", dueDate: "Tonight"),
        Task(name: "Prepare for family gathering", dueDate: "Friday"),
        Task(name: "Pay utility bills", dueDate: "Saturday"),
        Task(name: "Visit the grandparents", dueDate: "Sunday")
    ]

    var body: some View {
        VStack {
            headerListingView
                .padding(.horizontal)
            ScrollView {
                VStack(spacing: 10) {
                    tasksListingView
                }
                .padding(.horizontal)
            }
            .padding(.vertical)
        }
        .padding()
        .background(Color(UIColor.systemGray6))
    }
}

extension TaskManagerHomeView {
    private var tasksListingView: some View {
        VStack(spacing: 10) {
            ForEach(tasks) { task in
                taskCardView(task: task)
            }
        }
    }

    private func taskCardView(task: Task) -> some View {
        VStack(alignment: .leading) {
            HStack {
         
                Text(task.name)
                    .font(.system(size: 18, weight: .medium))
                    .foregroundColor(.primary)
                Spacer()
                Text(task.dueDate)
                    .font(.system(size: 16, weight: .regular))
                    .foregroundColor(.secondary)
                
            }
            .padding(.vertical, 8)
        }
        .padding(.horizontal)
        .padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))
        .background(Color(UIColor.systemBackground))
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: 2)
    }

    private var headerListingView: some View {
        VStack {
            Text("Task Manager")
                .font(.largeTitle)
                .fontWeight(.bold)
                .padding(.bottom, 15)
            VStack {
                HStack {
                    Text("Task")
                        .font(.headline)
                    Spacer()
                    Text("Due Date")
                        .font(.headline)
                }
                 Divider()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TaskManagerHomeView()
    }
}












